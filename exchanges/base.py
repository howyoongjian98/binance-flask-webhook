from flask_classful import FlaskView, route
import json
from flask import request
from exchanges import config
from exchanges.custom_symbols import CUSTOM_SYMBOLS
from multiprocessing import Process


class BaseExchangeFlaskView(FlaskView):
    def before_request(self, *args, **kwargs):
        data = json.loads(request.data)
        if data['passphrase'] != config.WEBHOOK_PASSPHRASE:
            return {
                "code": "error",
                "message": "Nice try, invalid passphrase"
            }

        del data['passphrase']
        self.data = data

    @route('/split_limit_orders/', methods=['POST'])
    def split_limit_orders_route(self):
        self.split_limit_orders(**self.data)

        return "Orders placed"

    @route('/split_close_limit_orders/', methods=['POST'])
    def split_close_limit_orders_route(self):
        self.split_close_limit_orders(**self.data)

        return "Orders placed"

    @route('/close_at_market_post/', methods=['POST'])
    def close_at_market_post_route(self):
        def handle_request(data):
            # convert custom symbols into list & recurse
            if isinstance(data['symbol'], str):
                if data['symbol'].lower() in CUSTOM_SYMBOLS:
                    new_data = []

                    custom_symbol = data['symbol'].lower()
                    long_data = {
                        **data, 'symbol': CUSTOM_SYMBOLS[custom_symbol][self.exchange_id].get('long')}
                    new_data.append(long_data)

                    if CUSTOM_SYMBOLS[custom_symbol][self.exchange_id].get('short'):
                        short_data = {**data, 'symbol': CUSTOM_SYMBOLS[custom_symbol][self.exchange_id]['short']}
                        new_data.append(short_data)

                    [handle_request(x) for x in new_data]

                else:
                    # final destination
                    self.close_at_market_post(**data)

            # loop over list & handle single symbol
            if isinstance(data['symbol'], list):
                symbol = data['symbol']
                jobs = []
                for individual_symbol in symbol:
                    new_data = {
                        **self.data, 'symbol': individual_symbol}
                    p = Process(target=handle_request,
                                args=(new_data,))
                    p.start()
                    jobs.append(p)
                [job.join() for job in jobs]

        handle_request(self.data)

        return "Position closed"

    @route('/open_at_market_post/', methods=['POST'])
    def open_at_market_post_route(self):
        def handle_request(data):
            # convert custom symbols into list & recurse
            if isinstance(data['symbol'], str):
                if data['symbol'].lower() in CUSTOM_SYMBOLS:
                    new_data = []

                    custom_symbol = data['symbol'].lower()
                    long_data = {
                        **data, 'symbol': CUSTOM_SYMBOLS[custom_symbol][self.exchange_id].get('long')}
                    new_data.append(long_data)

                    if CUSTOM_SYMBOLS[custom_symbol][self.exchange_id].get('short'):
                        order_type = 'sell' if data['order_type'] == 'buy' else 'buy'
                        short_data = {**data, 'symbol': CUSTOM_SYMBOLS[custom_symbol][self.exchange_id]['short'],
                                      'order_type': order_type}
                        new_data.append(short_data)
                    [handle_request(x) for x in new_data]
                else:
                    # final destination
                    self.open_at_market_post(**data)

            # loop over list & handle single symbol
            if isinstance(data['symbol'], list):
                symbol = data['symbol']
                jobs = []
                amount_usd = round(data['amount_usd'] / len(symbol), 2)
                for individual_symbol in symbol:
                    new_data = {
                        **data, 'symbol': individual_symbol, 'amount_usd': amount_usd}
                    p = Process(target=handle_request,
                                args=(new_data,))
                    p.start()
                    jobs.append(p)
                [job.join() for job in jobs]

        handle_request(self.data)

        return "Position opened"
