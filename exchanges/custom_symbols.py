# custom symbols that acts like ETFs


CUSTOM_SYMBOLS = {
    'foan': {
        'ftx': {
            'long': ['ONE-PERP', 'FTM-PERP', 'ATOM-PERP', 'NEAR-PERP']
        }
    },
    'foan-btc': {
        'ftx': {
            'long': ['ONE-PERP', 'FTM-PERP', 'ATOM-PERP', 'NEAR-PERP'],
            'short': ['BTC-PERP']
        }
    },
    'solunavax': {
        'ftx': {
            'long': ['SOL-PERP', 'LUNA-PERP', 'AVAX-PERP'],
        }
    },
    'arfil': {
        'ftx': {
            'long': ['AR-PERP'],
            'short': ['FIL-PERP']
        }
    }
}
