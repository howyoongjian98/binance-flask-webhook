
from exchanges.bybit import Bybit, BybitView
from unittest.mock import call
from pprint import pprint
import json


class TestBybitRetryUntilLimitFill():
    def setup(self):
        self.long_ticker = {'ask': 1.01,
                            'askVolume': None,
                            'average': 1.0123,
                            'baseVolume': 6445981.12,
                            'bid': 1.0099,
                            'bidVolume': None,
                            'change': -0.0045999999999999375,
                            'close': 1.01,
                            'datetime': '2021-12-31T10:40:23.740Z',
                            'high': 1.0487,
                            'info': {'ask_price': '1.01',
                                     'bid_price': '1.0099',
                                     'countdown_hour': '6',
                                     'delivery_fee_rate': '',
                                     'delivery_time': '',
                                     'funding_rate': '0.000837',
                                     'high_price_24h': '1.0487',
                                     'index_price': '1.0085',
                                     'last_price': '1.0100',
                                     'last_tick_direction': 'ZeroPlusTick',
                                     'low_price_24h': '0.8950',
                                     'mark_price': '1.0091',
                                     'next_funding_time': '2021-12-31T16:00:00Z',
                                     'open_interest': '1695967',
                                     'open_value': '0.00',
                                     'predicted_delivery_price': '',
                                     'predicted_funding_rate': '0.001261',
                                     'prev_price_1h': '0.9698',
                                     'prev_price_24h': '1.0146',
                                     'price_1h_pcnt': '0.041451',
                                     'price_24h_pcnt': '-0.004533',
                                     'symbol': 'DUSKUSDT',
                                     'total_turnover': '65792806.97',
                                     'total_volume': '82953522',
                                     'turnover_24h': '6445981.12',
                                     'volume_24h': '6655947'},
                            'last': 1.01,
                            'low': 0.895,
                            'open': 1.0146,
                            'percentage': -0.4533,
                            'previousClose': None,
                            'quoteVolume': 6655947.0,
                            'symbol': 'DUSK/USDT',
                            'timestamp': 1640947223740,
                            'vwap': 1.0325731453585145
                            }

        self.sample_test_long_position = {'auto_add_margin': '0',
                                          'bust_price': '0.746',
                                          'cum_realised_pnl': '92.7595074',
                                          'deleverage_indicator': '2',
                                          'entry_price': '1.01698437',
                                          'free_qty': '-24000',
                                          'is_isolated': False,
                                          'leverage': '12.5',
                                          'liq_price': '0.7867',
                                          'mode': 'BothSide',
                                          'occ_closing_fee': '13.428',
                                          'position_idx': '1',
                                          'position_margin': '6504.01962597',
                                          'position_value': '24407.6249',
                                          'realised_pnl': '-7.74012096',
                                          'risk_id': '1541',
                                          'side': 'Buy',
                                          'size': '24000',
                                          'stop_loss': '0',
                                          'symbol': 'DUSKUSDT',
                                          'take_profit': '0',
                                          'tp_sl_mode': 'Full',
                                          'trailing_stop': '0',
                                          'unrealised_pnl': '84.3751',
                                          'user_id': '1960148'}

        self.sample_test_long_order = {'amount': 10.0,
                                       'average': None,
                                       'clientOrderId': None,
                                       'cost': 0.0,
                                       'datetime': None,
                                       'fee': {'cost': 0.0, 'currency': 'DUSK'},
                                       'fees': [{'cost': 0.0, 'currency': 'DUSK'}],
                                       'filled': 0.0,
                                       'id': '9996ed31-a0d8-4bd1-8dd6-f898d07e8fe3',
                                       'info': {'close_on_trigger': False,
                                                'created_time': '2021-12-31T11:28:20Z',
                                                'cum_exec_fee': '0',
                                                'cum_exec_qty': '0',
                                                'cum_exec_value': '0',
                                                'last_exec_price': '0',
                                                'order_id': '9996ed31-a0d8-4bd1-8dd6-f898d07e8fe3',
                                                'order_link_id': '',
                                                'order_status': 'Created',
                                                'order_type': 'Limit',
                                                'position_idx': '1',
                                                'price': '1.0209',
                                                'qty': '10',
                                                'reduce_only': False,
                                                'side': 'Buy',
                                                'sl_trigger_by': 'UNKNOWN',
                                                'stop_loss': '0',
                                                'symbol': 'DUSKUSDT',
                                                'take_profit': '0',
                                                'time_in_force': 'GoodTillCancel',
                                                'tp_trigger_by': 'UNKNOWN',
                                                'updated_time': '2021-12-31T11:28:20Z',
                                                'user_id': '1960148'},
                                       'lastTradeTimestamp': None,
                                       'post_only': False,
                                       'price': 1.0209,
                                       'remaining': 10.0,
                                       'side': 'buy',
                                       'status': 'open',
                                       'stopPrice': None,
                                       'symbol': 'DUSK/USDT',
                                       'timeInForce': 'GTC',
                                       'timestamp': None,
                                       'trades': [],
                                       'type': 'limit'}

        self.long_close_order = {'info': {'id': '106715851626', 'clientId': None, 'market': 'LUNA-PERP', 'type': 'limit', 'side': 'buy', 'price': '40.0', 'size': '1.0', 'status': 'new', 'filledSize': '0.0', 'remainingSize': '1.0', 'reduce_only': False, 'liquidation': None, 'avgFillPrice': None, 'post_only': False, 'ioc': False, 'createdAt': '2021-12-21T07:39:04.381948+00:00', 'future': 'LUNA-PERP'},
                                 'id': '106715851626', 'clientOrderId': None, 'timestamp': 1640072344381, 'datetime': '2021-12-21T07:39:04.381Z', 'lastTradeTimestamp': None, 'symbol': 'LUNA-PERP', 'type': 'limit', 'timeInForce': None, 'post_only': False, 'side': 'buy', 'price': 40.0, 'stopPrice': None, 'amount': 1.0, 'cost': 0.0, 'average': None, 'filled': 0.0, 'remaining': 1.0, 'status': 'open', 'fee': None, 'trades': [], 'fees': []}

        self.sample_test_short_position_small_ticker = {'info': {'future': 'SMALL-PERP', 'size': '5509600', 'side': 'sell', 'netSize': '5509600', 'longOrderSize': '0.0', 'shortOrderSize': '-0.0', 'cost': '104332.14', 'entryPrice': '0.0181500', 'unrealizedPnl': '0.0', 'realizedPnl': '1468.9675516', 'initialMarginRequirement': '0.05', 'maintenanceMarginRequirement': '0.03', 'openSize': '5509600', 'collateralUsed': '5216.607', 'estimatedLiquidationPrice': '65.52503267763937', 'recentAverageOpenPrice': '0.0181500', 'recentPnl': '5762.6214', 'recentBreakEvenPrice': '0.0181500',
                                                                 'cumulativeBuySize': '2322.0', 'cumulativeSellSize': '1057.0'},
                                                        'symbol': 'TEST-PERP', 'timestamp': None, 'datetime': None, 'initialMargin': 5216.607, 'initialMarginPercentage': 0.05, 'maintenanceMargin': 3129.9642, 'maintenanceMarginPercentage': 0.03, 'entryPrice': None, 'notional': 104332.14, 'leverage': 20, 'unrealizedPnl': 5762.6214, 'contracts': 5509600, 'contractSize': 1.0, 'marginRatio': 0.1273, 'liquidationPrice': 65.52503267763937, 'markPrice': 0.0181500, 'collateral': 24572.937862786195, 'marginType': 'cross', 'side': 'short', 'percentage': 110.46
                                                        }

        self.params = {
            "post_only": True,
            "reduce_only": True
        }

    def test_retry_limit_order_until_fill_1(self, mocker):
        # order that fills with 0 retry

        mocker.patch('exchanges.bybit.Bybit._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.bybit.Bybit.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.return_value = {
            **self.long_close_order, 'status': 'closed'}

        Bybit()._retry_limit_order_until_fill('TEST-PERP', 'sell', 24000, self.params)
        exchange().createLimitOrder.assert_called_once_with(
            'TEST-PERP', 'sell', 24000, 1.01, self.params)

    def test_retry_limit_order_until_fill_2(self, mocker):
        # order that fills with 1 retry
        mocker.patch('exchanges.bybit.Bybit._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.bybit.Bybit.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.side_effect = [self.long_close_order, {
            **self.long_close_order, 'status': 'closed'}, {**self.long_close_order, 'status': 'closed'}]

        Bybit()._retry_limit_order_until_fill('TEST-PERP', 'sell', 24000, self.params)
        assert exchange().createLimitOrder.call_args_list == [call(
            'TEST-PERP', 'sell', 24000, 1.01, self.params), call('TEST-PERP', 'sell', 24000, 1.01, self.params)]

    def test_retry_limit_order_until_fill_3(self, mocker):
        # order that fills with 1 retry & 1st order partial fill
        mocker.patch('exchanges.bybit.Bybit._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.bybit.Bybit.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.side_effect = [self.long_close_order, {
            **self.long_close_order, 'filled': 1000}, {**self.long_close_order, 'status': 'closed'}]

        Bybit()._retry_limit_order_until_fill('TEST-PERP', 'sell', 24000, self.params)
        assert exchange().createLimitOrder.call_args_list == [call(
            'TEST-PERP', 'sell', 24000, 1.01, self.params), call('TEST-PERP', 'sell', 23000, 1.01, self.params)]

    def test_retry_limit_order_until_fill_4(self, mocker):
        # order that updates latest ticker price
        mocker.patch('exchanges.bybit.Bybit._fetch_ticker', side_effect=[self.long_ticker, {
                     **self.long_ticker, 'ask': 163.345}, {**self.long_ticker, 'ask': 163.345}])
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.bybit.Bybit.get_exchange')
        exchange.return_value.createLimitOrder.side_effect = [
            self.long_close_order, {**self.long_close_order}]
        exchange.return_value.fetchOrder.side_effect = [
            self.long_close_order, self.long_close_order, {**self.long_close_order, 'status': 'closed'}]

        Bybit()._retry_limit_order_until_fill('TEST-PERP', 'sell', 24000, self.params)
        assert exchange().createLimitOrder.call_args_list == [call(
            'TEST-PERP', 'sell', 24000, 1.01, self.params), call('TEST-PERP', 'sell', 24000, 163.345, self.params)]


class TestBybitCloseMarket():
    def setup(self):
        self.sample_test_long_position = {'auto_add_margin': '0',
                                          'bust_price': '0.746',
                                          'cum_realised_pnl': '92.7595074',
                                          'deleverage_indicator': '2',
                                          'entry_price': '1.01698437',
                                          'free_qty': '-24000',
                                          'is_isolated': False,
                                          'leverage': '12.5',
                                          'liq_price': '0.7867',
                                          'mode': 'BothSide',
                                          'occ_closing_fee': '13.428',
                                          'position_idx': '1',
                                          'position_margin': '6504.01962597',
                                          'position_value': '24407.6249',
                                          'realised_pnl': '-7.74012096',
                                          'risk_id': '1541',
                                          'side': 'Buy',
                                          'size': '24000',
                                          'stop_loss': '0',
                                          'symbol': 'DUSKUSDT',
                                          'take_profit': '0',
                                          'tp_sl_mode': 'Full',
                                          'trailing_stop': '0',
                                          'unrealised_pnl': '84.3751',
                                          'user_id': '1960148'}

        self.sample_test_long_order = {'amount': 10.0,
                                       'average': None,
                                       'clientOrderId': None,
                                       'cost': 0.0,
                                       'datetime': None,
                                       'fee': {'cost': 0.0, 'currency': 'DUSK'},
                                       'fees': [{'cost': 0.0, 'currency': 'DUSK'}],
                                       'filled': 0.0,
                                       'id': '9996ed31-a0d8-4bd1-8dd6-f898d07e8fe3',
                                       'info': {'close_on_trigger': False,
                                                'created_time': '2021-12-31T11:28:20Z',
                                                'cum_exec_fee': '0',
                                                'cum_exec_qty': '0',
                                                'cum_exec_value': '0',
                                                'last_exec_price': '0',
                                                'order_id': '9996ed31-a0d8-4bd1-8dd6-f898d07e8fe3',
                                                'order_link_id': '',
                                                'order_status': 'Created',
                                                'order_type': 'Limit',
                                                'position_idx': '1',
                                                'price': '1.0209',
                                                'qty': '10',
                                                'reduce_only': False,
                                                'side': 'Buy',
                                                'sl_trigger_by': 'UNKNOWN',
                                                'stop_loss': '0',
                                                'symbol': 'DUSKUSDT',
                                                'take_profit': '0',
                                                'time_in_force': 'GoodTillCancel',
                                                'tp_trigger_by': 'UNKNOWN',
                                                'updated_time': '2021-12-31T11:28:20Z',
                                                'user_id': '1960148'},
                                       'lastTradeTimestamp': None,
                                       'post_only': False,
                                       'price': 1.0209,
                                       'remaining': 10.0,
                                       'side': 'buy',
                                       'status': 'open',
                                       'stopPrice': None,
                                       'symbol': 'DUSK/USDT',
                                       'timeInForce': 'GTC',
                                       'timestamp': None,
                                       'trades': [],
                                       'type': 'limit'}

    def test_close_at_market_post_1(self, mocker):
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch(
            'exchanges.bybit.Bybit._retry_limit_order_until_fill')
        Bybit().close_at_market_post('TEST-PERP', 1)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', 24000, params)

    def test_close_at_market_post_2(self, mocker):
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch(
            'exchanges.bybit.Bybit._retry_limit_order_until_fill')
        Bybit().close_at_market_post('TEST-PERP', 0.5)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', 24000/2, params)

    def test_close_at_market_post_3(self, mocker):
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch(
            'exchanges.bybit.Bybit._retry_limit_order_until_fill')
        Bybit().close_at_market_post('TEST-PERP', 0.1)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', 24000/10, params)

    def test_close_at_market_post_4(self, mocker):
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch(
            'exchanges.bybit.Bybit._retry_limit_order_until_fill')
        Bybit().close_at_market_post('TEST-PERP', 0.01)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', 24000/100, params)

    def test_close_at_market_post_5(self, mocker):
        mocker.patch('exchanges.bybit.Bybit._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch(
            'exchanges.bybit.Bybit._retry_limit_order_until_fill')
        Bybit().close_at_market_post('TEST-PERP', 0.0625)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', 1500, params)
