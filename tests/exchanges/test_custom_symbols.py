from exchanges.custom_symbols import CUSTOM_SYMBOLS


def test_custom_symbols():
    for i in CUSTOM_SYMBOLS.items():
        # assert custom symbol == lowercase
        assert i[0] == i[0].lower()
        for j in i[1].items():
            # assert exchange_id == lowercase
            assert j[0] == j[0].lower()
            for k in j[1].items():
                assert k[0] == k[0].lower()
                assert k[0] == 'long' or k[0] == 'short'
                for l in k[1]:
                    # assert exchange symbol == uppercase
                    assert l == l.upper()
