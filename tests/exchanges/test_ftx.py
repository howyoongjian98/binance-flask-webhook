
from exchanges.ftx import FTX
from unittest.mock import call

class TestFTXRetryUntilLimitFill():
    def setup(self):
        self.long_ticker = {'symbol': 'LUNA-PERP', 'timestamp': 1640073033018, 'datetime': '2021-12-21T07:50:33.018Z', 'high': None, 'low': None, 'bid': 81.203, 'bidVolume': None, 'ask': 81.208, 'askVolume': None, 'vwap': None, 'open': 76.4451356389566, 'close': 81.2, 'last': 81.2, 'previousClose': None, 'change': 4.7548643610433965, 'percentage': 5.855744286999257, 'average': None, 'baseVolume': None, 'quoteVolume': 600845135.1394, 'info': {'name': 'LUNA-PERP', 'enabled': True,
                                                                                                                                                                                                                                                                                                                                                                                                                                                              'postOnly': False, 'priceIncrement': '0.001', 'sizeIncrement': '0.1', 'minProvideSize': '0.1', 'last': '81.2', 'bid': '81.203', 'ask': '81.208', 'price': '81.203', 'type': 'future', 'baseCurrency': None, 'quoteCurrency': None, 'underlying': 'LUNA', 'restricted': False, 'highLeverageFeeExempt': False, 'change1h': '-0.01306546099807968', 'change24h': '0.05855744286999257', 'changeBod': '0.004950311250819895', 'quoteVolume24h': '600845135.1394', 'volumeUsd24h': '600845135.1394'}}
        self.sample_test_long_position = {'info': {'future': 'TEST-PERP', 'size': '1265.0', 'side': 'buy', 'netSize': '1265.0', 'longOrderSize': '0.0', 'shortOrderSize': '-0.0', 'cost': '104332.14', 'entryPrice': '82.476', 'unrealizedPnl': '0.0', 'realizedPnl': '1468.9675516', 'initialMarginRequirement': '0.05', 'maintenanceMarginRequirement': '0.03', 'openSize': '1265.0', 'collateralUsed': '5216.607', 'estimatedLiquidationPrice': '65.52503267763937', 'recentAverageOpenPrice': '79.20230749354005', 'recentPnl': '5762.6214', 'recentBreakEvenPrice': '77.9205680632411',
                                                   'cumulativeBuySize': '2322.0', 'cumulativeSellSize': '1057.0'},
                                          'symbol': 'TEST-PERP', 'timestamp': None, 'datetime': None, 'initialMargin': 5216.607, 'initialMarginPercentage': 0.05, 'maintenanceMargin': 3129.9642, 'maintenanceMarginPercentage': 0.03, 'entryPrice': None, 'notional': 104332.14, 'leverage': 20, 'unrealizedPnl': 5762.6214, 'contracts': 1265.0, 'contractSize': 1.0, 'marginRatio': 0.1273, 'liquidationPrice': 65.52503267763937, 'markPrice': 82.476, 'collateral': 24572.937862786195, 'marginType': 'cross', 'side': 'long', 'percentage': 110.46
                                          }
        self.long_close_order = {'info': {'id': '106715851626', 'clientId': None, 'market': 'LUNA-PERP', 'type': 'limit', 'side': 'buy', 'price': '40.0', 'size': '1.0', 'status': 'new', 'filledSize': '0.0', 'remainingSize': '1.0', 'reduceOnly': False, 'liquidation': None, 'avgFillPrice': None, 'postOnly': False, 'ioc': False, 'createdAt': '2021-12-21T07:39:04.381948+00:00', 'future': 'LUNA-PERP'},
                            'id': '106715851626', 'clientOrderId': None, 'timestamp': 1640072344381, 'datetime': '2021-12-21T07:39:04.381Z', 'lastTradeTimestamp': None, 'symbol': 'LUNA-PERP', 'type': 'limit', 'timeInForce': None, 'postOnly': False, 'side': 'buy', 'price': 40.0, 'stopPrice': None, 'amount': 1.0, 'cost': 0.0, 'average': None, 'filled': 0.0, 'remaining': 1.0, 'status': 'open', 'fee': None, 'trades': [], 'fees': []}
        self.short_ticker = FTX()._fetch_ticker('SPELL-PERP')

        self.sample_test_short_position_small_ticker = {'info': {'future': 'SMALL-PERP', 'size': '5509600', 'side': 'sell', 'netSize': '5509600', 'longOrderSize': '0.0', 'shortOrderSize': '-0.0', 'cost': '104332.14', 'entryPrice': '0.0181500', 'unrealizedPnl': '0.0', 'realizedPnl': '1468.9675516', 'initialMarginRequirement': '0.05', 'maintenanceMarginRequirement': '0.03', 'openSize': '5509600', 'collateralUsed': '5216.607', 'estimatedLiquidationPrice': '65.52503267763937', 'recentAverageOpenPrice': '0.0181500', 'recentPnl': '5762.6214', 'recentBreakEvenPrice': '0.0181500',
                                                                 'cumulativeBuySize': '2322.0', 'cumulativeSellSize': '1057.0'},
                                                        'symbol': 'TEST-PERP', 'timestamp': None, 'datetime': None, 'initialMargin': 5216.607, 'initialMarginPercentage': 0.05, 'maintenanceMargin': 3129.9642, 'maintenanceMarginPercentage': 0.03, 'entryPrice': None, 'notional': 104332.14, 'leverage': 20, 'unrealizedPnl': 5762.6214, 'contracts': 5509600, 'contractSize': 1.0, 'marginRatio': 0.1273, 'liquidationPrice': 65.52503267763937, 'markPrice': 0.0181500, 'collateral': 24572.937862786195, 'marginType': 'cross', 'side': 'short', 'percentage': 110.46
                                                        }
        self.params = {
            "postOnly": True,
            "reduceOnly": True
        }

    def test_retry_limit_order_until_fill_1(self, mocker):
        # order that fills with 0 retry

        mocker.patch('exchanges.ftx.FTX._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.ftx.FTX.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.return_value = {**self.long_close_order, 'status': 'closed'}

        FTX()._retry_limit_order_until_fill('TEST-PERP', 'sell', 1265, self.params)
        exchange().createLimitOrder.assert_called_once_with('TEST-PERP', 'sell', 1265, 81.208, self.params)

    def test_retry_limit_order_until_fill_2(self, mocker):
        # order that fills with 1 retry
        mocker.patch('exchanges.ftx.FTX._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.ftx.FTX.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.side_effect = [self.long_close_order, {**self.long_close_order, 'status': 'closed'}, {**self.long_close_order, 'status': 'closed'}]

        FTX()._retry_limit_order_until_fill('TEST-PERP', 'sell', 1265, self.params)
        assert exchange().createLimitOrder.call_args_list == [call('TEST-PERP', 'sell', 1265, 81.208, self.params), call('TEST-PERP', 'sell', 1265, 81.208, self.params)]

    def test_retry_limit_order_until_fill_3(self, mocker):
        # order that fills with 1 retry & 1st order partial fill
        mocker.patch('exchanges.ftx.FTX._fetch_ticker',
                     return_value=self.long_ticker)
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.ftx.FTX.get_exchange')
        exchange.return_value.createLimitOrder.return_value = self.long_close_order
        exchange.return_value.fetchOrder.side_effect = [{**self.long_close_order, 'filled': 265}, {**self.long_close_order, 'status': 'closed'}]
 
        FTX()._retry_limit_order_until_fill('TEST-PERP', 'sell', 1265, self.params)
        assert exchange().createLimitOrder.call_args_list == [call('TEST-PERP', 'sell', 1265, 81.208, self.params), call('TEST-PERP', 'sell', 1000, 81.208, self.params)]

    def test_retry_limit_order_until_fill_4(self, mocker):
        # order that updates latest ticker price
        mocker.patch('exchanges.ftx.FTX._fetch_ticker', side_effect=[self.long_ticker, {**self.long_ticker, 'ask': 163.345}, {**self.long_ticker, 'ask': 163.345}] )
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)

        exchange = mocker.patch('exchanges.ftx.FTX.get_exchange')
        exchange.return_value.createLimitOrder.side_effect = [self.long_close_order, {**self.long_close_order}]
        exchange.return_value.fetchOrder.side_effect = [self.long_close_order, {**self.long_close_order, 'status': 'closed'}]

        FTX()._retry_limit_order_until_fill('TEST-PERP', 'sell', 1265, self.params)
        assert exchange().createLimitOrder.call_args_list == [call('TEST-PERP', 'sell', 1265, 81.208, self.params), call('TEST-PERP', 'sell', 1265, 163.345, self.params)]

class TestFTXCloseMarket():
    def setup(self):
        self.sample_test_long_position = {'info': {'future': 'TEST-PERP', 'size': '1265.0', 'side': 'buy', 'netSize': '1265.0', 'longOrderSize': '0.0', 'shortOrderSize': '-0.0', 'cost': '104332.14', 'entryPrice': '82.476', 'unrealizedPnl': '0.0', 'realizedPnl': '1468.9675516', 'initialMarginRequirement': '0.05', 'maintenanceMarginRequirement': '0.03', 'openSize': '1265.0', 'collateralUsed': '5216.607', 'estimatedLiquidationPrice': '65.52503267763937', 'recentAverageOpenPrice': '79.20230749354005', 'recentPnl': '5762.6214', 'recentBreakEvenPrice': '77.9205680632411',
                                                   'cumulativeBuySize': '2322.0', 'cumulativeSellSize': '1057.0'},
                                          'symbol': 'TEST-PERP', 'timestamp': None, 'datetime': None, 'initialMargin': 5216.607, 'initialMarginPercentage': 0.05, 'maintenanceMargin': 3129.9642, 'maintenanceMarginPercentage': 0.03, 'entryPrice': None, 'notional': 104332.14, 'leverage': 20, 'unrealizedPnl': 5762.6214, 'contracts': 1265.0, 'contractSize': 1.0, 'marginRatio': 0.1273, 'liquidationPrice': 65.52503267763937, 'markPrice': 82.476, 'collateral': 24572.937862786195, 'marginType': 'cross', 'side': 'long', 'percentage': 110.46
                                          }

    def test_close_at_market_post_1(self, mocker):
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch('exchanges.ftx.FTX._retry_limit_order_until_fill')
        FTX().close_at_market_post('TEST-PERP', 1)
        close_size = 1265
        params = {
            "postOnly": True,
            "reduceOnly": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', close_size, params)

    def test_close_at_market_post_2(self, mocker):
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch('exchanges.ftx.FTX._retry_limit_order_until_fill')
        FTX().close_at_market_post('TEST-PERP', 0.5)
        close_size = 632.5
        params = {
            "postOnly": True,
            "reduceOnly": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', close_size, params)

    def test_close_at_market_post_3(self, mocker):
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch('exchanges.ftx.FTX._retry_limit_order_until_fill')
        FTX().close_at_market_post('TEST-PERP', 0.1)
        close_size = 126.5
        params = {
            "postOnly": True,
            "reduceOnly": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', close_size, params)

    def test_close_at_market_post_4(self, mocker):
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch('exchanges.ftx.FTX._retry_limit_order_until_fill')
        FTX().close_at_market_post('TEST-PERP', 0.01)
        close_size = 12.65
        params = {
            "postOnly": True,
            "reduceOnly": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', close_size, params)

    def test_close_at_market_post_5(self, mocker):
        mocker.patch('exchanges.ftx.FTX._fetch_position',
                     return_value=self.sample_test_long_position)
        stub = mocker.patch('exchanges.ftx.FTX._retry_limit_order_until_fill')
        FTX().close_at_market_post('TEST-PERP', 0.0625)
        close_size = 79.0625
        params = {
            "postOnly": True,
            "reduceOnly": True
        }
        stub.assert_called_once_with('TEST-PERP', 'sell', close_size, params)
